package com.example.kushansachintha.notesapp;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class DbAccessor extends ContentProvider {

    private static final String AUTHORITY = "com.example.kushansachintha.notesapp";
    private static final String BASE_PATH = "notes";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    //constatnt to identify the requested operations
    private static final int NOTES = 1;
    private static final int NOTES_ID = 2;

    public static final String CONTENT_ITEM_TYPE = "note";

    private static final UriMatcher urimatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        urimatcher.addURI(AUTHORITY,BASE_PATH,NOTES);
        urimatcher.addURI(AUTHORITY,BASE_PATH + "/#", NOTES);
    }

    private SQLiteDatabase sqldb;

    @Override
    public boolean onCreate() {
        SqlHelper helper = new SqlHelper(getContext());
        sqldb = helper.getWritableDatabase();

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {

        if (urimatcher.match (uri) == NOTES_ID){
            selection = SqlHelper.NOTE_ID + "=" + uri.getLastPathSegment();
        }
        return sqldb.query(SqlHelper.TABLE_NOTES, SqlHelper.ALL_COLUMNS, selection,null,null,null, SqlHelper.NOTE_CREATED);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long id = sqldb.insert(SqlHelper.TABLE_NOTES,null, values);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return sqldb.delete(SqlHelper.TABLE_NOTES, selection, selectionArgs);
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return sqldb.update(SqlHelper.TABLE_NOTES, values, selection, selectionArgs);
    }
}
