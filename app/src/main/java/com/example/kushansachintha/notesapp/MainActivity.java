package com.example.kushansachintha.notesapp;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final int EDITOR_CONSTANT = 1234;
    private CursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] from = {SqlHelper.NOTE_TEXT};
        int[] to = {R.id.noteTextView};
        adapter = new SimpleCursorAdapter(this, R.layout.custom_list,null,from,to,0);
        ListView listView = (ListView)findViewById(android.R.id.list);
        listView.setAdapter(adapter);

        getLoaderManager().initLoader(0,null,this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, NoteCreatorActivity.class);
                Uri uri = Uri.parse(DbAccessor.CONTENT_URI + "/" + id);
                intent.putExtra(DbAccessor.CONTENT_ITEM_TYPE, uri);
                startActivityForResult(intent, EDITOR_CONSTANT);
            }
        });

        getLoaderManager().initLoader(0,null,this);
    }


    private void insertNote(String note) {
        ContentValues values = new ContentValues();
        values.put(SqlHelper.NOTE_TEXT, note /*4*/ );
        Uri noteUri = getContentResolver().insert(DbAccessor.CONTENT_URI, values);

        Log.d("MainAtivity","Note Inserted" + noteUri.getLastPathSegment());
    }

    public void addNewNote(View view){
        Intent intent = new Intent(this, NoteCreatorActivity.class);
        startActivityForResult(intent, EDITOR_CONSTANT);
    }

    private void createNotes() {
        insertNote("These are sample notes");
        insertNote("This is a Lengthy note that exceeds screen size limit of the device");
        insertNote("Two-line\nnote");
        refreshView();
    }


    private void deleteAllNotes() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialogInterface, int button){
                if (button == DialogInterface.BUTTON_POSITIVE){
                    getContentResolver().delete(DbAccessor.CONTENT_URI,null,null);
                    Toast.makeText(MainActivity.this,getString(R.string.All_notes_cleared),Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(MainActivity.this,getString(R.string.Operation_cancelled),Toast.LENGTH_SHORT).show();
                refreshView();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.confirm)).
                setPositiveButton(getString(android.R.string.yes),dialogClickListener).
                setNegativeButton(getString(android.R.string.no),dialogClickListener).
                show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        switch (id){
            case R.id.create_notes:
                createNotes();
                break;

            case R.id.delete_all_notes:
                deleteAllNotes();
                break;
        }

         return super.onOptionsItemSelected(item);
    }


    private void refreshView() {

        getLoaderManager().restartLoader(0,null,this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDITOR_CONSTANT && resultCode == RESULT_OK){
            refreshView();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this, DbAccessor.CONTENT_URI,null,null,null,null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
