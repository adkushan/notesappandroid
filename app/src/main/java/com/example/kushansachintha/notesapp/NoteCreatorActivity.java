package com.example.kushansachintha.notesapp;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class NoteCreatorActivity extends AppCompatActivity {

    private String doing;
    private EditText editor;
    private String noteFilter;
    private String prevNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_creator);
        editor = (EditText) findViewById(R.id.editText);

        Intent intent = getIntent();
        Uri uri = intent.getParcelableExtra(DbAccessor.CONTENT_ITEM_TYPE);
        if (uri == null){
            doing = Intent.ACTION_INSERT;
            setTitle(getString(R.string.add_new_note));
        }
        else {
            doing = Intent.ACTION_EDIT;
            setTitle(getString(R.string.edit_note));
            noteFilter = SqlHelper.NOTE_ID + "=" +uri.getLastPathSegment();
            Cursor cursor = getContentResolver().query(uri,SqlHelper.ALL_COLUMNS,noteFilter,null,null);
            cursor.moveToFirst();
            prevNote = cursor.getString(cursor.getColumnIndex(SqlHelper.NOTE_TEXT));
            editor.setText(prevNote);
            editor.requestFocus();
        }
    }

    private void addNote(String note) {
        ContentValues values = new ContentValues();
        values.put(SqlHelper.NOTE_TEXT, note );
        getContentResolver().insert(DbAccessor.CONTENT_URI, values);
        setResult(RESULT_OK);
    }

    private void updateNote(String note) {
        ContentValues values = new ContentValues();
        values.put(SqlHelper.NOTE_TEXT, note );
        getContentResolver().update(DbAccessor.CONTENT_URI, values, noteFilter,null);
        Toast.makeText(this, getString(R.string.updated),Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);

    }

    private void deleteNote() {
        getContentResolver().delete(DbAccessor.CONTENT_URI, noteFilter, null);
        Toast.makeText(this, R.string.deleted, Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    private void editEnd(){

        String typedtext = editor.getText().toString().trim();
        switch (doing){
            case Intent.ACTION_INSERT:
                if (typedtext.length() ==0){
                    setResult(RESULT_CANCELED);
                }
                else {
                    addNote(typedtext);
                }
                break;
            case Intent.ACTION_EDIT:
                if (typedtext.length() == 0){
                    deleteNote();
                }
                else
                    updateNote(typedtext);
        }

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (doing.equals(Intent.ACTION_EDIT)){
            getMenuInflater().inflate(R.menu.menu_mgr, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()){
            case android.R.id.home:
                editEnd();
                break;
            case R.id.delete:
                deleteNote();
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed(){
        editEnd();
    }
}
